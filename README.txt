README for Front page redirect module
-------------------------------------

@author: Erwin Derksen (fietserwin: http://drupal.org/user/750928)
@link: http://drupal.org/sandbox/fietserwin/1621130 (project page)


Introduction
------------
The front page redirect module redirects visitors that arrive on the front page
of your site to the page that is defined as the default front page.

Drupal allows you to define what content to serve on the home page. However,
Drupal does not redirect visitors to that page but instead just serves the
content of that page on the front page.

This might lead to some problems:
- Same content under different URLs (/ and /{site_frontpage}).
- Different content under the same URL, e.g. when using different front pages
  for different languages.
- The previous point may lead to #339958: Cached pages returned in wrong
  language when browser language used.
- No possibility to pack keywords in the URL of your front page.

To alleviate these problems, this module really redirects visitors that arrive
at the front page to the page that is defined as front page. It does so using a
301 (Moved Permanently) status code.
 

Targeted use
------------
This module is typically targeted at:
- multilingual sites
- that have a different homepage per language (i18n_variable module from i18n)
- and use the browser settings to detect the language (locale module from core).


Installing
----------
As usual.


Configuration and usage
-----------------------
There is no separate configuration for this module. This modules uses the
configuration as set in field "Default front page" on the "Site information"
page (admin/config/system/site-information). 
 
If you want a different front page per language, install the i18n project
(http://drupal.org/project/i18n) and enable the "Variable translation"
(i18n_variable) sub module. On the "Multilingual settings variables" page
(admin/config/regional/i18n/variable) you have to check the "Default front page"
checkbox and save your settings.

Then go back to the "Site information" page and you will see that there is a
fieldset at the top of the page telling you "There are multilingual variables in
this form." Select the language you want to define the front page for and fill
it in the "Default front page" field.
