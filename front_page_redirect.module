<?php
/**
 * @file
 * Front page redirect module file.
 *
 * I tried to to this in hook_language_init() but then common.inc has not yet
 * been loaded, so no:
 * - drupal_goto(): difficult to overcome: I would need to create the redirect
 *   URL myself without the option of looking up aliases and execute hooks like
 *   url_outbound and drupal_goto.
 * - drupal_get_query_parameters(): could be overcome.
 */

/**
 * Implements hook_init.
 */
function front_page_redirect_init() {
  // We need to find out if this is a "normal front page" request.
  // 1) Look at the script: it should end with /index.php, (/index.php5 or
  //   something like that also allowed), and should not be cron.php,
  //   update.php, authorize.php, etc. I use a "white list" check on index.php,
  //   not a black list on the others as the list is not exhaustive and users
  //   may even add their own.
  // 2) Do not redirect when running in a cli.
  $script = $_SERVER['PHP_SELF'];
  if (strripos($script, '/index.php') !== strripos($script, '/') || drupal_is_cli()) {
    return;
  }

  // 3) We use request_path() to get info about the current request as it always
  //   returns a "clean URL" without base path, index.php and query string, even
  //   if ?q=/path is used. @see request_path().
  //   For a request for the home page that has to be redirected, $request_path
  //   must be empty or equal to the language prefix. (Note: if it also equals
  //   the alias of an existing internal path, the language prefix takes
  //   precedence.)
  // Note: The Global redirect module redirects from en/welcome back to /en,
  //   so the check on $request_path being equal to $language->prefix may have
  //   to be removed if this leads to problems.
  $request_path = request_path();
  global $language;
  if (($request_path === '' || $request_path === $language->prefix)) {
    // Add a vary header when the home page is a multilingual variable
    // and when language detection was based on the browser settings.
    if (function_exists('i18n_variable_list') && i18n_variable_list('site_frontpage')
      && $language->provider === LOCALE_LANGUAGE_NEGOTIATION_BROWSER) {
      header('Vary: Accept-Language', FALSE);
    }
    // This redirect may be cached and is permanent.
    header('Cache-Control: public', TRUE);
    // Remove the Expires header that gets added by Drupal.
    header_remove('Expires');
    $frontpage = variable_get('site_frontpage', 'node');
    $query_array = drupal_get_query_parameters();
    drupal_goto($frontpage, array('query' => $query_array), 301);
  }
}
